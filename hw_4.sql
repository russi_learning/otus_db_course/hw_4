/*
Перед тем как создать табличное пространство нужно создать директорию и дать права для postgres
1) sudo mkdir /data/dbs
2) chown postgres:postgres /data/dbs
*/

-- Создаем табличное пространство
CREATE TABLESPACE dbspace LOCATION '/data/dbs';

-- Создаем пользователя
CREATE USER shop_user;

-- Для добавления пароля
ALTER USER shop_user WITH password '123';

-- Создаем базу данных в созданном tablespace
CREATE DATABASE shop TABLESPACE dbspace;

-- Даем права на базу данных пользователю shop_user;
GRANT ALL ON DATABASE shop TO shop_user;

/*
Выполняется в psql
Для подключения к бд shop пользователем shop_user
\c shop shop_user localhost
*/

-- Создаем схему для пользовательской информации
CREATE SCHEMA customer_info;

-- Создаем схему для информации по продукту
CREATE SCHEMA product_info;

-- Создаем схему для информации по провайдеру
CREATE SCHEMA provider_info;

-- Создаем product_info.category
CREATE TABLE IF NOT EXISTS product_info.category
(
    id SERIAL PRIMARY KEY,
    NAME VARCHAR(80) NOT NULL UNIQUE
) TABLESPACE dbspace;

ALTER TABLE product_info.category OWNER TO shop_user;

-- Создаем таблицу для контактов
CREATE TABLE IF NOT EXISTS customer_info.contacts
(
    id SERIAL PRIMARY KEY,
    email VARCHAR(100) NOT NULL UNIQUE,
    phone VARCHAR(15)
) TABLESPACE dbspace;

ALTER TABLE customer_info.contacts OWNER TO shop_user;

-- Создаем таблицу для информации о покупателе
CREATE TABLE IF NOT EXISTS customer_info.customer
(
    id SERIAL PRIMARY KEY,
    name       VARCHAR(50) NOT NULL,
    last_name  VARCHAR(80) NOT NULL,
    contact_id INTEGER     NOT NULL
        CONSTRAINT fk_contact
            REFERENCES customer_info.contacts
            ON DELETE CASCADE
) TABLESPACE dbspace;

ALTER TABLE customer_info.customer OWNER TO shop_user;

-- Создаем таблицу для информации о производителе товара
CREATE TABLE IF NOT EXISTS product_info.producer
(
    id SERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL UNIQUE
) TABLESPACE dbspace;

ALTER TABLE product_info.producer OWNER TO shop_user;

-- Создаем таблицу с информацией о продукте
CREATE TABLE IF NOT EXISTS product_info.products
(
    id SERIAL PRIMARY KEY,
    title       VARCHAR(100) NOT NULL,
    description VARCHAR(500) NOT NULL,
    category_id INTEGER      NOT NULL
        CONSTRAINT fk_category
            REFERENCES product_info.category
            ON DELETE SET NULL,
    producer_id INTEGER NOT NULL
        CONSTRAINT fk_producer
            REFERENCES product_info.producer
            ON DELETE CASCADE
) TABLESPACE dbspace;

ALTER TABLE product_info.products OWNER TO shop_user;

-- Создаем таблицу с информацией о поставщике
CREATE TABLE IF NOT EXISTS provider_info.provider
(
    id SERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL UNIQUE
) TABLESPACE dbspace;

ALTER TABLE provider_info.provider OWNER TO shop_user;

-- Создаем таблицу с информацией о цене на продукт
CREATE TABLE IF NOT EXISTS product_info.price
(
    id SERIAL PRIMARY KEY,
    value INTEGER NOT NULL CONSTRAINT price_value_check CHECK (value > 0),
    product_id INTEGER NOT NULL CONSTRAINT fk_product REFERENCES product_info.products ON DELETE CASCADE,
    provider_id INTEGER NOT NULL CONSTRAINT fk_provider REFERENCES provider_info.provider ON DELETE CASCADE
) TABLESPACE dbspace;

ALTER TABLE product_info.price OWNER TO shop_user;

-- Создаем таблицу с информацией о покупке
CREATE TABLE IF NOT EXISTS customer_info.purchase
(
    id SERIAL PRIMARY KEY,
    customer_id INTEGER NOT NULL
        CONSTRAINT fk_customer
            REFERENCES customer_info.customer
            ON DELETE CASCADE,
    price_id INTEGER NOT NULL
        CONSTRAINT fk_price
            REFERENCES product_info.price
            ON DELETE CASCADE
) TABLESPACE dbspace;

ALTER TABLE customer_info.purchase OWNER TO shop_user;

-- Создаем таблицу с информацией о запасах продуктов у поставщика
CREATE TABLE IF NOT EXISTS provider_info.stock_goods
(
    id SERIAL PRIMARY KEY,
    goods_balance INTEGER NOT NULL
        CONSTRAINT stock_goods_goods_balance_check
            CHECK (goods_balance >= 0),
    product_id INTEGER NOT NULL
        CONSTRAINT fk_product
            REFERENCES product_info.products
            ON DELETE CASCADE,
    provider_id INTEGER NOT NULL
        REFERENCES provider_info.provider
            ON DELETE CASCADE
) TABLESPACE dbspace;

ALTER TABLE provider_info.stock_goods OWNER TO shop_user;